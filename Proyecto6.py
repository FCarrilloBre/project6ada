# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:29:06 2019
Programacion dinamica, minimos cuadrados segmentados
@author: fcarr_000
"""

import math
import random
import matplotlib.pyplot as pyplot

#solucion de minimos cuadrados de un segmento de linea
#regresa a=pendiente, b=y-interseccion, e=error cuadrado

def mincuadcoef(xarr,yarr):
    n=len(xarr)
    if (n == 1):
        return (0.0, yarr[0], 0.0)
    sx = sum(xarr)
    sy = sum(yarr)
    sx2 = sum([x*x for x in xarr])
    sxy = 0
    for i in range(n):
        sxy = sxy + xarr[i]*yarr[i]
    a = (n*sxy-sx*sy)/(n*sx2-sx*sx)    
    b = (sy-a*sx)/n
    e = 0
    for i in range(n):
        e = e + (yarr[i]-a*xarr[i]-b)*(yarr[i]-a*xarr[i]-b)
    return (a,b,e)

#Precalcula todos los coeficientes minimos cuadrados para todos los pares de puntos
#El resultado es una lista de listas, una por j, cada entrada en una sublista es 
#una tupla para una i
#cada tupla es el coeficiente a, b el error para el segmento i,j
def precalculo(n,xarray,yarray):
    result = []
    for j in range(n):
        jres = []
        for i in range(0,j+1):
            a,b,e = mincuadcoef(xarray[i:j+1],yarray[i:j+1])     
            jres = jres + [(a,b,e)]
        result = result + [jres]
    return (result)       

#estima la varianza de un arreglo y para usarse en el calculo del segmento
#al encontrar una solucion optima
def estvariance(n,yarray):
    meany = sum(yarray)/n
    sqrdiffy = [(y-meany)*(y-meany) for y in yarray]
    yvar = sum(sqrdiffy)/n
    return yvar
    
#soulucion de programacion dinamica usando los resultados precalculados

def findopt(n,preresult,C):
    optresult = []   
    for j in range(0,n):
        beste = 9e999
        besti = -1
        jpre = preresult[j]   #lista de tuplas (a,b,c) para i=0,1,...,j
        #para cada posible inicio i, hacia j
        for i in range(0,j+1):
            #tomar el erroe asumiendo el uso de opt de i-1, y nuevo fit para i..j,
            #con penalidad por segmento C
            if (i > 0):
                e = jpre[i][2] + optresult[i-1][0] + C
            else:
                e = jpre[i][2] + C
            #encontrar i con el error mas pequeño          
            if (e < beste):
                beste = e
                besti = i
                
        #crear entrada opt para j, que consiste en un error mínimo y una lista de (i, j)
        #esta es una lista de segmentos (i, j) para este j, que consiste en el mejor actual (i, j) 
        #agregado a la lista de opt [besti-1]
        if (besti > 0):
            optresult = optresult + [[beste, optresult[besti-1][1] + [(besti,j)]]] 
        else:
            optresult = optresult + [[beste, [(besti,j)]]]
    return optresult[n-1]
    
#construir una línea ajustada a partir de una solución óptima 
def constructfit(n,xarray,yarray,preresult,opt):
    yfit = []
    optintervals = opt[1]    
    #Para cada segmento   
    for interval in optintervals:
        i=interval[0]        #tomar el segmento
        j=interval[1]
        a = preresult[j][i][0]    #tomar los coeficientes de ese segmento
        b = preresult[j][i][1]
        xarr = xarray[i:j+1]
        yfit = yfit + [a*x+b for x in xarr]   #construir ajuste
    return yfit
    
    
#minimos cuadrados segmentados
def segls(n,xarray,yarray,Cfactor):
    preresult = precalculo(n,xarray,yarray)  #precalcula coeficientes y errores
    yvar = estvariance(n,yarray)
    C = Cfactor * yvar                       #penalidad para agregar segmentos
    opt = findopt(n,preresult,C)             #solucion optima
    yfit = constructfit(n,xarray,yarray,preresult,opt)   #calculo ajuste  
    return yfit

#generate data
def gendata():
    n=50
    xarray = range(50)
    yarray = []
    for x in xarray:
        noise = 20*random.random()
        yarray = yarray + [(noise*x)/2]
    return (n,xarray,yarray)

n,xarray,yarray = gendata()
pyplot.figure(1)
pyplot.plot(xarray,yarray)

Cfactor = 0.1      #factor en el calculo de penalidad para agregar segmentos

#minimos cuadrados segmentados
yfit =segls(n,xarray,yarray,Cfactor)

pyplot.figure(2)
pyplot.plot(xarray,yarray,xarray,yfit)
pyplot.show()

Cfactor2 = 0.5      #factor en el calculo de penalidad para agregar segmentos

#minimos cuadrados segmentados
yfit2 =segls(n,xarray,yarray,Cfactor2)

pyplot.figure(3)
pyplot.plot(xarray,yarray,xarray,yfit2)
pyplot.show()

Cfactor3 = 0.8      #factor en el calculo de penalidad para agregar segmentos

#minimos cuadrados segmentados
yfit3 =segls(n,xarray,yarray,Cfactor3)

pyplot.figure(4)
pyplot.plot(xarray,yarray,xarray,yfit3)
pyplot.show()